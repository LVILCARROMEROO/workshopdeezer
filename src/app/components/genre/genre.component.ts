import {Component, Input, OnInit} from '@angular/core';
import {Genre} from '../../interfaces/genre';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.scss']
})
export class GenreComponent implements OnInit {
  @Input() genre: Genre;

  constructor() { }

  ngOnInit() {
  }

}
