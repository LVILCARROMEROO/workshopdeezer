import { Injectable } from '@angular/core';
import { GenresResponse} from '../interfaces/genre';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChartResponse} from '../interfaces/chart';

@Injectable({
  providedIn: 'root'
})
export class DeezerService {

  constructor(
    private http: HttpClient
  ) { }

  getGenres(): Observable<GenresResponse> {
    // Use Moesif Orign & CORS Changer (chrome extension)
    return this.http.get('https://api.deezer.com/genre');
  }

  getChart(genreId): Observable<ChartResponse> {
    // return this.http.get('https://api.deezer.com/chart/' + genreId);
    return this.http.get(`https://api.deezer.com/chart/${genreId}`);
  }
}
