import { Component, OnInit } from '@angular/core';
import {Genre} from '../../interfaces/genre';
import {DeezerService} from '../../services/deezer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {
  genres: Genre[] = [];

  constructor(
    private deezer: DeezerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.deezer.getGenres()
      .subscribe(
        (res) => {
          this.genres = res.data;
        },
        (err) => { console.log(err); },
      );
  }

  goChart(genreId) {
    // this.router.navigateByUrl('/chart/' + genreId);
    this.router.navigate(['/chart', genreId]);
  }

}
