import { Component, OnInit } from '@angular/core';
import {DeezerService} from '../../services/deezer.service';
import {ActivatedRoute} from '@angular/router';
import {Album, Artist, Playlist, Track} from '../../interfaces/chart';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  albums: Album[];
  artists: Artist[];
  playlists: Playlist[];
  tracks: Track[];

  constructor(
    private deezer: DeezerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const genreId = this.route.snapshot.paramMap.get('genreId');
    this.deezer.getChart(genreId)
      .subscribe(
        (res) => {
          this.albums = res.albums.data;
          this.artists = res.artists.data;
          this.playlists = res.playlists.data;
          this.tracks = res.tracks.data;
        },
        (err) => {
          console.log(err);
        },
      );
  }

}
