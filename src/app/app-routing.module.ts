import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GenresComponent} from './pages/genres/genres.component';
import {ChartComponent} from './pages/chart/chart.component';

const routes: Routes = [
  { path: '', redirectTo: 'genres', pathMatch: 'full' },
  { path: 'genres', component: GenresComponent },
  { path: 'chart/:genreId', component: ChartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
